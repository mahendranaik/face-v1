package mrk1.com.mark1;

import android.util.Log;

public class Util{
    public static final String PREF_FILE_DATA = "mrk1.com.mark1.repf_file.account_data";
    public static final String ACCOUNT = "ACCOUNT";
    public static final String AADHAR = "AADHAR";
    public static final String USER_NAME = "USER_NAME";
    public static final String ACCOUNT_CONFIGURED = "ACCOUNT_CONFIGURED";
    public static final String INT_ACCOUNT = "INT_ACCOUNT";
    public static final String WITH_ACCOUNT = "WITH_ACCOUNT";

    public static final String IMAGE_INTENT = "IMAGE_INTENT";
    public static final String TAKE_PICTURE_INTENT = "TAKE_PICTURE";
    public static final String VERIFY_PICTURE_INTENT = "VERIFY_PICTURE";
    private static boolean DEBUG = true;

    public static final String SERVER = "http://74.82.31.60:8080/";
    public static void log_debug(String TAG, String message){
        if(DEBUG) {
            Log.d(TAG, message);
        }
    }

    public static void log_error(String TAG, String message){
        Log.e(TAG,message);
    }
}