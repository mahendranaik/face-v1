package mrk1.com.mark1;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    private static final String MODEL_FILE = "file:///android_asset/frozen_tfdroid.pb";
    private static final String INPUT_NODE = "I";
    private static final String OUTPUT_NODE = "O";

    private static final long[] INPUT_SIZE = {1,3};
    private Button newaccount;
    private Button signin;
    private TextView textView;
    private TextView text2;
    private Button verify;
    private RelativeLayout newLayout;
    private TextInputLayout accLayout;
    private TensorFlowInferenceInterface inferenceInterface;
    private static final String LOGTAG = MainActivity.class.getSimpleName();
    static {
        System.loadLibrary("tensorflow_inference");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inferenceInterface = new TensorFlowInferenceInterface(getAssets(), MODEL_FILE);
        verify = (Button) findViewById(R.id.button);
        newaccount = (Button) findViewById(R.id.new_account);
        signin = (Button) findViewById(R.id.signin);
        textView = (TextView) findViewById(R.id.textView);
        text2 = (TextView) findViewById(R.id.text2);
        newLayout = (RelativeLayout)findViewById(R.id.newLayout);
        accLayout = (TextInputLayout)findViewById(R.id.accountLayout);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FaceDetectionActivity.class));
            }
        });
        newaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AccountCreateActivity.class));
            }
        });
        signin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //TODO:
                final String _account_num = accLayout.getEditText().getText().toString();
                if(TextUtils.isEmpty(_account_num) || !TextUtils.isDigitsOnly(_account_num)){
                    accLayout.setError("Valid account number required");
                }else{
                    accLayout.setErrorEnabled(false);
                    AsyncHttpClient _client = new AsyncHttpClient();
                    RequestParams _rp = new RequestParams();
                    _rp.put("account_number", _account_num);
                    final ProgressDialog progress = new ProgressDialog(MainActivity.this);
                    progress.setMessage("Checking account ...");
                    progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progress.setIndeterminate(true);
                    progress.show();
                    _client.post(MainActivity.this, Util.SERVER+"/_check_account", _rp, new AsyncHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            progress.dismiss();
                            Toast.makeText(MainActivity.this, "Account exists. Access your accout using yopur face", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(MainActivity.this, FaceDetectionActivity.class)
                                    .putExtra(Util.WITH_ACCOUNT, true)
                            .putExtra(Util.INT_ACCOUNT, _account_num));
                            finish();
                            return;
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            progress.dismiss();
                            Toast.makeText(MainActivity.this, "Account check failed. Please try again", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    });
                }


            }
        });
        if(!accountConfigured()){
            textView.setVisibility(View.GONE);
            text2.setVisibility(View.GONE);
            verify.setVisibility(View.GONE);
            newLayout.setVisibility(View.VISIBLE);
        }else{
            textView.setVisibility(View.VISIBLE);
            text2.setVisibility(View.VISIBLE);
            verify.setVisibility(View.VISIBLE);
            newLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(accountConfigured()) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_main, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.item_logout:
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Are you sure?");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.USER_NAME, "").commit();
                getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.ACCOUNT, "").commit();
                getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.AADHAR, "").commit();
                getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putBoolean(Util.ACCOUNT_CONFIGURED, false).commit();
                MainActivity.this.recreate();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
    private boolean accountConfigured(){
        String _tempAcnt = getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).getString(Util.ACCOUNT, "");
        String _tempAadhar = getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).getString(Util.AADHAR, "");
        String _tempName = getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).getString(Util.USER_NAME, "");
        if(TextUtils.isEmpty(_tempAadhar) || TextUtils.isEmpty(_tempAcnt)){
            Util.log_error(LOGTAG,"user is not registered");
            return false;
        }
        else{
            return true;
        }
    }
}
