
package mrk1.com.mark1;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

public class FaceVerifyActivity extends AppCompatActivity {

    private ImageView faceView;
    private TextView faceErr;
    private FancyButton verifyButton;
    private Toolbar my_toolbar;
    private static final String LOGTAG = FaceVerifyActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_verify);
        faceView = (ImageView)findViewById(R.id.faceView);
        faceErr = (TextView)findViewById(R.id.facesError);
        verifyButton = (FancyButton)findViewById(R.id.picVerify);
        my_toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(my_toolbar);
    }
    private static boolean with_only_account = false;
    private static String intent_account = "";
    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().hide();
        with_only_account = getIntent().getBooleanExtra(Util.WITH_ACCOUNT, true);
        intent_account = getIntent().getStringExtra(Util.INT_ACCOUNT);
        final String image_path = getIntent().getStringExtra("PATH");
        Bitmap bMap = BitmapFactory.decodeFile(image_path);
        faceView.setImageBitmap(bMap);
        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncHttpClient _client = new AsyncHttpClient();
                RequestParams _rp = new RequestParams();
                if(!with_only_account) {
                    _rp.add("account_number", getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).getString(Util.ACCOUNT, ""));
                }else{
                    _rp.add("account_number",intent_account);
                }
                _rp.put("with_account", with_only_account?1:0);
                 File dest = new File(image_path);
                if(dest.exists()) {
                    try {
                        _rp.put("photo", dest);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Util.log_error(LOGTAG, "File upload failed");
                        Toast.makeText(FaceVerifyActivity.this, "Photo upload failed", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                final ProgressDialog progress = new ProgressDialog(FaceVerifyActivity.this);
                progress.setMessage("Verifying user ...");
                progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progress.setIndeterminate(true);
                progress.show();
                _client.post(FaceVerifyActivity.this,
                        Util.SERVER+"_predict",
                        _rp, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progress.dismiss();
                                float _prob = -1;
                                //update shared preferences with the user data
                                String _response = new String(responseBody);
                                Util.log_debug(LOGTAG, "response, :"+_response);
                                try {
                                    JSONObject _obj = new JSONObject(_response);
                                    String _user_name = _obj.getString("name");
                                    String _aadhar = _obj.getString("aadhar");
                                    String _probability = _obj.getString("probability");
                                    _prob = Float.parseFloat(_probability);
                                    Util.log_debug(LOGTAG, "_user_name, :"+_user_name+" , aadhar:"+_aadhar+" , prob:"+_probability);
                                    getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.USER_NAME, _user_name).commit();
                                    getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.ACCOUNT, intent_account).commit();
                                    getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.AADHAR, _aadhar).commit();
                                    getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putBoolean(Util.ACCOUNT_CONFIGURED, false).commit();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(FaceVerifyActivity.this,
                                        "User verified "+((_prob != -1)?":"+String.format("%.2f", _prob):""),
                                        Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(FaceVerifyActivity.this, MainActivity.class));
                                finish();
                                return;
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                progress.dismiss();
                                error.printStackTrace();
                                Util.log_error(LOGTAG, "Error_code:"+statusCode);
                                Toast.makeText(FaceVerifyActivity.this, "User verification failed. Please try again!!", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }
}
