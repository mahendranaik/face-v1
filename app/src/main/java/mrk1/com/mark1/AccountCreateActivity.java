package mrk1.com.mark1;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;

public class AccountCreateActivity extends AppCompatActivity {

    private ImageView _photoView;
    private TextInputLayout _username;
    private TextInputLayout _accountNo;
    private TextInputLayout _aadharNum;
    private Button submitButton;
    private Button uploadButton;
    private Toolbar my_toolbar;
    private String _imagePath;
    private static final String LOGTAG=AccountCreateActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_create);
        my_toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(my_toolbar);
        _photoView = (ImageView)findViewById(R.id.photo);
        _username = (TextInputLayout)findViewById(R.id.nameLayout);
        _aadharNum = (TextInputLayout) findViewById(R.id.aadharLayout);
        _accountNo = (TextInputLayout) findViewById(R.id.accountLayout);
        submitButton = (Button)findViewById(R.id.submitButton);
        uploadButton = (Button)findViewById(R.id.uploadImage);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent _i = new Intent(AccountCreateActivity.this, TakePictureActivity.class);
                startActivity(_i);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        File sd = Environment.getExternalStorageDirectory();
        Util.log_debug(LOGTAG, sd.getPath());
        File dest = new File(sd, "tmp_prof_pic.png");
        if(dest.exists()) {
            Picasso.with(AccountCreateActivity.this).invalidate(dest);
            Picasso.with(AccountCreateActivity.this).load(dest).into(_photoView);
            uploadButton.setText("Change");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_user, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.item_cancel:
                finish();
                return true;
            case R.id.item_save:
                createAccount();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isEmail(String _emailstr)
    {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(_emailstr).matches();
    }
    private void createAccount()
    {
        final String _name = _username.getEditText().getText().toString();
        final String _account = _accountNo.getEditText().getText().toString();
        final String _aadhar = _aadharNum.getEditText().getText().toString();

        if(TextUtils.isEmpty(_name)){
            _username.setError("User name cannot be empty");
            return;
        }else{
            _username.setErrorEnabled(false);
        }
        if(TextUtils.isEmpty(_account)){
            _accountNo.setError("Account number required");
            return;
        }else if(!TextUtils.isDigitsOnly(_account)){
            _accountNo.setError("Invalid Accoutn number");
            return;
        }else{
            _accountNo.setErrorEnabled(false);
        }
        if(TextUtils.isEmpty(_aadhar)){
            _aadharNum.setError("Aadhar number required");
            return;
        }else if(!TextUtils.isDigitsOnly(_aadhar) || _aadhar.length() != 12){
            _aadharNum.setError("Invalid Aadhar number");
            return;
        }else{
            _aadharNum.setErrorEnabled(false);
        }
        Util.log_debug(LOGTAG, "name:"+_name);
        Util.log_debug(LOGTAG, "account:"+_account);
        Util.log_debug(LOGTAG, "aadhar:"+_aadhar);

        File sd = Environment.getExternalStorageDirectory();
        Util.log_debug(LOGTAG, sd.getPath());
        File dest = new File(sd, "tmp_prof_pic.png");
        if(!dest.exists()) {
            Toast.makeText(AccountCreateActivity.this, "Please upload a profile picture", Toast.LENGTH_SHORT).show();
            return;
        }
//        if(TextUtils.isDigitsOnly(_imagePath)){
//            Toast.makeText(AccountCreateActivity.this, "Upload user image", Toast.LENGTH_SHORT).show();
//            return;
//        }

        AsyncHttpClient _client = new AsyncHttpClient();
        RequestParams _rp = new RequestParams();
        _rp.add("aadhar_number", _aadhar);
        _rp.add("account_number", _account);
        _rp.add("full_name", _name);

//        File sd = Environment.getExternalStorageDirectory();
//        Util.log_debug(LOGTAG, sd.getPath());
//        File dest = new File(sd, "tmp_prof_pic.png");
        if(dest.exists()) {
            //_rp.put("photo", dest);
            try {
                _rp.put("photo", dest);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Util.log_error(LOGTAG, "File upload failed");
                Toast.makeText(AccountCreateActivity.this, "Photo upload failed", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        final ProgressDialog progress = new ProgressDialog(AccountCreateActivity.this);
        progress.setMessage("Registering user ...");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.show();
        _client.post(AccountCreateActivity.this,
                Util.SERVER+"_add_user",
                _rp, new FileAsyncHttpResponseHandler(AccountCreateActivity.this) {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                        progress.dismiss();
                        throwable.printStackTrace();
                        Util.log_error(LOGTAG, "Error_code:"+statusCode);
                        Toast.makeText(AccountCreateActivity.this, "User registration failed. Please try again!!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, File file) {
                        progress.dismiss();
                        Toast.makeText(AccountCreateActivity.this, "User registraation successful", Toast.LENGTH_SHORT).show();
                        //update shared preferences with the user data
                        getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.USER_NAME, _name).commit();
                        getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.ACCOUNT, _account).commit();
                        getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putString(Util.AADHAR, _aadhar).commit();
                        getSharedPreferences(Util.PREF_FILE_DATA, Context.MODE_PRIVATE).edit().putBoolean(Util.ACCOUNT_CONFIGURED, false).commit();
                        return;
                    }
                });
        return;
    }
}
