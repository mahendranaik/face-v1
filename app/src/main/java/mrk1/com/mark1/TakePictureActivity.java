package mrk1.com.mark1;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import mehdi.sakout.fancybuttons.FancyButton;
import mrk1.com.mark1.ui.camera.CameraSourcePreview;
import mrk1.com.mark1.ui.camera.FaceGraphic;
import mrk1.com.mark1.ui.camera.GraphicOverlay;

public class TakePictureActivity extends AppCompatActivity {
    private static final String LOGTAG = TakePictureActivity.class.getSimpleName();
    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    private CameraSource mCameraSource = null;
    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;
    private TextView faceErr;
    private FancyButton _clkBtn;
    private FaceDetector detector = null;
    private FaceDetector face_detector = null;
    private static int num_faces = 0;
    private Toolbar _toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_picture);
        _toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(_toolbar);
        faceErr = (TextView)findViewById(R.id.facesError);
        _clkBtn = (FancyButton)findViewById(R.id.picVerify);
        _clkBtn.setEnabled(false);
        faceErr.setVisibility(View.GONE);
        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);

        _clkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture("tmp_prof_pic.png");
            }
        });
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if ( ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }
    }

    private void takePicture(final String image_path)
    {
        final ProgressDialog progress = new ProgressDialog(TakePictureActivity.this);
        progress.setMessage("Extracting image...");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.show();
        mCameraSource.takePicture(new CameraSource.ShutterCallback() {
            @Override
            public void onShutter() {

            }
        }, new CameraSource.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] bytes) {
                //Picture taken
                Util.log_debug(LOGTAG, "picture taken");
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                SparseArray<Face> faces = face_detector.detect(frame);
                if(faces.size() == 1){
                    Face face = faces.valueAt(0);
                    PointF _f = face.getPosition();
                    float _yPos = face.getEulerY();
                    float _rPos = face.getEulerZ();
                    if(!((_yPos > -10 && _yPos < 10 ) &&( _rPos > -10 && _rPos < 10))){
                        Toast.makeText(TakePictureActivity.this, "Face the camera", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    float _fWidth = face.getWidth();
                    float _fHeight = face.getHeight();
                    float crp_wdth = 200;
                    float crp_height = 180;
                    if(_fWidth < crp_wdth){
                        crp_wdth = _fWidth;
                        crp_height = _fHeight;
                    }else{
                        crp_height = (_fHeight/_fWidth) * crp_wdth;
                    }
                    Matrix _tf = new Matrix();
                    _tf.setScale((float)crp_wdth/_fWidth, (float)crp_height/ _fHeight);
                    Bitmap _scaled = Bitmap.createBitmap(bitmap, (int)_f.x, (int)_f.y, (int)_fWidth, (int)_fHeight, _tf, true);
                    File sd = Environment.getExternalStorageDirectory();
                    Util.log_debug(LOGTAG, sd.getPath());
                    File dest = new File(sd, image_path);
                    try {
                        FileOutputStream out = new FileOutputStream(dest);
                        _scaled.compress(Bitmap.CompressFormat.PNG, 90, out);
                        out.flush();
                        out.close();
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        progress.dismiss();
    }


    private void createCameraSource() {

        Context context = getApplicationContext();
        detector = new FaceDetector.Builder(context)
                .setMode(FaceDetector.ACCURATE_MODE)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();

        face_detector = new FaceDetector.Builder(context)
                .setTrackingEnabled(false)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());

        if (!detector.isOperational()) {
            // Note: The first time that an app using face API is installed on a device, GMS will
            // download a native library to the device in order to do detection.  Usually this
            // completes before the app is run for the first time.  But if that download has not yet
            // completed, then the above call will not detect any faces.
            //
            // isOperational() can be used to check if the required native library is currently
            // available.  The detector will automatically become operational once the library
            // download completes on device.
            Util.log_debug(LOGTAG, "Face detector dependencies are not yet available.");
        }

        mCameraSource = new CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(640, 480)
                .setAutoFocusEnabled(true)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(30.0f)
                .build();
    }

    private void requestCameraPermission(){
        Util.log_debug(LOGTAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Util.log_debug(LOGTAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Util.log_debug(LOGTAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        Util.log_error(LOGTAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Mark1")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    /**
     * Restarts the camera.
     */
    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];

    private final float[] mRotationMatrix = new float[9];
    private final float[] mOrientationAngles = new float[3];
    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
        if(_toolbar != null) {
            getSupportActionBar().hide();
        }
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Util.log_error(LOGTAG, "Unable to start camera source.");
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
            num_faces++;
            Handler _h = new Handler(Looper.getMainLooper());
            _h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (num_faces == 1) {
                                _clkBtn.setEnabled(true);
                                faceErr.setVisibility(View.GONE);
                            } else if (num_faces > 1) {
                                _clkBtn.setEnabled(false);
                                faceErr.setText("Multiple faces found!!!");
                                faceErr.setVisibility(View.VISIBLE);
                            } else if (num_faces < 1) {
                                _clkBtn.setEnabled(false);
                                faceErr.setText("No faces found!!!");
                                faceErr.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }, 250);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
            num_faces--;
            Util.log_debug(LOGTAG, "Faces removed");
            Handler _h = new Handler(Looper.getMainLooper());
            _h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (num_faces == 1) {
                                _clkBtn.setEnabled(true);
                                faceErr.setVisibility(View.GONE);
                            } else if (num_faces > 1) {
                                _clkBtn.setEnabled(false);
                                faceErr.setText("Multiple faces found!!!");
                                faceErr.setVisibility(View.VISIBLE);
                            } else if (num_faces < 1) {
                                _clkBtn.setEnabled(false);
                                faceErr.setText("No faces found!!!");
                                faceErr.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }, 250);
            Util.log_error(LOGTAG, "Num faces:" + num_faces);
        }
    }
}
